# Terraform example on GCP

## **Work in progress**

An example of using Terraform with GCP.

Infrastructure is inspired by [this](https://medium.com/slalom-technology/a-complete-gcp-environment-with-terraform-c087190366f0) article. Many modifications were made as the code is not clear from this article (at least not to me). 

If you set up your credentials and authentication to GCP, this code will deploy.

## **TL;DR**
Run: `terraform plan` & `terraform apply`

## Architecture

_Picture in progress_

## Modules
### `global`
Will create:
- a VPC global network to deploy further infrastructure in
- a firewall rule allowing SSH traffic
- a firewall rule allowing HTTP traffic
- firewall rules allowing all internal traffic within the subnets

### `region`
Will create: 
- a private subnet
- a public subnet
- an instance in the public subnet, running nginx and serving an index.html with it's hostname and private ip-address.

### Variables
| variable | description| default | 
| --- | --- | --- |
| `project` | the GCP project id | "gcp-endeavours" |
| `env` | the environment | "dev" |
| `app` | the application name | "my-app-name" |
| `region` | the GCP region | "europe-west4" |
| `zone_1` | the first GCP zone within the region | "a" |
| `private_subnet_1` | CIDR block for private subnet in `zone_1` |"10.26.1.0/24" |
| `public_subnet_1` | CIDR block for private subnet in `zone_1` | "10.26.2.0/24" |
| `zone_2` | the second GCP zone within the region | "b" |
| `private_subnet_2` | CIDR block for private subnet in `zone_2` | "10.26.3.0/24" |
| `public_subnet_2` | CIDR block for private subnet in `zone_2` | "10.26.4.0/24" |
