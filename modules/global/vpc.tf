resource "google_compute_network" "vpc" {
	name 										= "${format("%s","${var.app}-${var.env}-vpc")}"
	auto_create_subnetworks = "false"
	routing_mode 						= "GLOBAL"
}

resource "google_compute_firewall" "allow-internal" {
	name 		= "${var.app}-fw-allow-internal"
	network = "${google_compute_network.vpc.name}"
	
	allow {
		protocol 	= "icmp"
	}
	allow {
		protocol 	= "tcp"
		ports 		= ["0-65535"]
	}
	allow {
		protocol 	= "udp"
		ports 		= ["0-65535"]
	}
	
	source_ranges = [
		"${var.var_public_subnet_1}",
		"${var.var_private_subnet_1}",
		"${var.var_public_subnet_2}",
		"${var.var_private_subnet_2}"
	]
}

resource "google_compute_firewall" "allow-http" {
	name 		= "${var.app}-fw-allow-http"
	network = "${google_compute_network.vpc.name}"

	allow {
		protocol 	= "tcp"
		ports 		= ["80"]
	}

	target_tags = ["http"]
}

resource "google_compute_firewall" "allow-bastion" {
	name = "${var.app}-fw-allow-bastion"
	network = "${google_compute_network.vpc.name}"

	allow {
		protocol 	= "tcp"
		ports 		= ["22"]
	}

	target_tags = ["ssh"]
}

