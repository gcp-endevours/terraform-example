variable "env" {
	type = "string"
}

variable "app" {
  type = "string"
}
variable "var_private_subnet_1" {
	type = "string"
}

variable "var_public_subnet_1" {
	type = "string"
}

variable "var_private_subnet_2" {
	type = "string"
}

variable "var_public_subnet_2" {
	type = "string"
}