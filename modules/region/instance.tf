resource "google_compute_instance" "default" {
	name          = "${format("%s","${var.app}-${var.env}-${var.region}-${var.zone}-instance")}"
	machine_type  = "f1-micro"
	zone          = "${format("%s","${var.region}-${var.zone}")}"
	tags          = ["ssh","http"]
	
	boot_disk {
		initialize_params {
			image = "ubuntu-minimal-1810-cosmic-v20190320"
		}
	}

	labels {
		webserver = "true"
	}

	metadata_startup_script = "apt update && apt -y install nginx && export HOSTNAME=$(hostname | tr -d '\n') && export PRIVATE_IP=$(curl -sf -H 'Metadata-Flavor:Google' http://metadata/computeMetadata/v1/instance/network-interfaces/0/ip | tr -d '\n') && rm /var/www/html/index.nginx-debian.html && touch /var/www/html/index.html && echo \"Welcome to $${HOSTNAME} - $${PRIVATE_IP}\" > /var/www/html/index.html && systemctl nginx.service start"
	
	network_interface {
		subnetwork = "${google_compute_subnetwork.public_subnet.name}"
	}
}
