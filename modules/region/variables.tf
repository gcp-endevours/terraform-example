variable "env" {
	type = "string"
}

variable "app" {
  type = "string"
}
variable "var_private_subnet" {
	type = "string"
}

variable "var_public_subnet" {
	type = "string"
}

variable "network_self_link" {
  type = "string"
}

variable "subnetwork" {
  type = "string"
}

variable "region" {
  type = "string"
}

variable "zone" {
  type = "string"
}