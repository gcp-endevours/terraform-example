output "out_public_subnet_name" {
  value = "${google_compute_subnetwork.public_subnet.name}"
}