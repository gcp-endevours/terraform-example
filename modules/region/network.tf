resource "google_compute_subnetwork" "public_subnet" {
	name 					= "${format("%s","${var.app}-${var.env}-${var.region}-${var.zone}-public-subnet")}"
	ip_cidr_range = "${var.var_public_subnet}"
	network       = "${var.network_self_link}"
	region        = "${var.region}"
}

resource "google_compute_subnetwork" "private_subnet" {
	name 					= "${format("%s","${var.app}-${var.env}-${var.region}-${var.zone}-private-subnet")}"
	ip_cidr_range = "${var.var_private_subnet}"
	network       = "${var.network_self_link}"
	region        = "${var.region}"
}
