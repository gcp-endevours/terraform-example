### General variables
variable "project" {
	default = "gcp-endeavours"
}

variable "env" {
	default = "dev"
}

variable "app" {
	default = "my-app-name"
}

variable "region" {
  default = "europe-west4"
}

### uc1 variables
variable "zone_1" {
  default = "a"
}

variable "private_subnet_1" {
	default = "10.26.1.0/24"
}

variable "public_subnet_1" {
	default = "10.26.2.0/24"
}

### ue1 variables
variable "zone_2" {
  default = "b"
}

variable "private_subnet_2" {
	default = "10.26.3.0/24"
}

variable "public_subnet_2" {
	default = "10.26.4.0/24"
}
