provider "google" {
	project	= "${var.project}"
  credentials = "${file("./gcp-endeavours-57f84753ef5a.json")}"
}

module "vpc" {
	source 									= "./modules/global"
	env											= "${var.env}"
	app 								    = "${var.app}"
	var_public_subnet_1 	  = "${var.public_subnet_1}"
	var_private_subnet_1   	= "${var.private_subnet_1}"
	var_public_subnet_2 	  = "${var.public_subnet_2}"
	var_private_subnet_2  	= "${var.private_subnet_2}"
}

module "region-deployment-1" {
	source 									= "./modules/region"
	env 										= "${var.env}"
  region                  = "${var.region}"
  zone                    = "${var.zone_1}"
	app 								    = "${var.app}"
	network_self_link 			= "${module.vpc.out_vpc_self_link}"
	subnetwork 						  = "${module.region-deployment-1.out_public_subnet_name}"
	var_public_subnet 	    = "${var.public_subnet_1}"
	var_private_subnet 	    = "${var.private_subnet_1}"
}

module "region-deployment-2" {
	source 									= "./modules/region"
	env 										= "${var.env}"
  region                  = "${var.region}"
  zone                    = "${var.zone_2}"
	app 								    = "${var.app}"
	network_self_link 			= "${module.vpc.out_vpc_self_link}"
	subnetwork 						  = "${module.region-deployment-2.out_public_subnet_name}"
	var_public_subnet 	    = "${var.public_subnet_2}"
	var_private_subnet 	    = "${var.private_subnet_2}"
}

##################################
# Display Output Public Instance #
##################################
